ThrustJS v0.1.9
===============

ThrustJS é um Web Container de execução/interpretação JavaScript, ou seja, é um Server-side JavaScript (SSJS).
Ele permite a escrita em JavaScript de códigos a ser interpretados pelo servidor Web, da mesma forma como ocorre com o PHP, por exemplo.

## What's new?

* v.0.1.9 - addSoftwareLibrary: uma *builtin function* que carrega dinamicamente JARs para o ambiente do thrustjs.
* v.0.1.8 - Liberado um REPL (ambiente iterativo) para o thrustjs.
* v.0.1.7 - Redefinição dos diretórios para priorizar e ordenar a carga dos módulos do thrustjs.
* v.0.1.5 - Load dinâmico de JARs a partir do WEB-INF\lib. Many bugs fixed.
* v.0.1.4 - Módulo de roteamento liberado.
* v.0.1.3 - Módulo de segurança liberado.
* v.0.1.2 - Módulo de acesso a base dados melhorado.
* v.0.1.1 - Carga dinâmica de módulos implementada.

## Tutorial

#### Inicializando um projeto ThrustJS
Baixe o conteúdo deste repositório e extraio-o para um diretório de sua preferência.

A seguinte estrutura será criada:

```
+-- app
|   --- index.html
+-- thrust
|   +-- app
|   +-- core
|       --- .platform.crypt.js
|       --- http.js
|   +-- config.json
|   +-- startup.js
--- thrust.jar
--- server.properties
```

### Entendendo a estrutura de diretórios
```
+-- app (diretório utilizado para servir os arquivos estáticos da aplicação)
|   --- index.html
+-- thrust (diretório principal do ThrusJS)
|   +-- app (diretório utilizado para armazenar o código backend da aplicação em si)
|   +-- core (diretório com o conteúdo mínimo para inicialização do ThrustJS)
|       --- .platform.crypt.js 
|       --- http.js 
|   --- config.json (arquivo que armazena valores de configurações)
|   --- startup.js (arquivo utilizado para indicar módulos à ser carregados no startup do ThrustJS)
--- thrust.jar (arquivo a ser utilizado para inicializar o ThrustJS)
--- server.properties (arquivo que armazena propriedades lidas pelo TomCat)
```

#### *Getting Started*
Vamos construir e disponibilizar um *endpoint* que retorna a String *Hello World*.
Caso o Thrust ainda não esteja em execução, execute-o através do seguinte comando:

```>> java -jar thrust.jar```

Crie um arquivo dentro do diretório thrust-seed/thrust/app chamado *test.js*.
Depois edite o arquivo *test.js*, crie uma função chamada *hello* e exporte-a utilizando a variável de módulo *exports*.

```javascript
/**
 * Serviço que responde a URI _/test/hello_
 * @param {Object} params - Objeto com os parâmetros enviados na requisição.
 * @param {http.Request} request - Wrapper da classe HttpServletRequest do Java.
 * @param {http.Response} response - Wrapper da classe HttpServletResponse do Java.
 */
function hello(params, request, response) {
    response.write("Hello World!");
}

exports = {
    hello: hello
}
```

Pronto! Ao salvar o arquivo *test.js* já temos um *endpoint* chamado *"hello"* criado e publicado.

Agora basta chamar a URL http://localhost:8080/test/hello a partir do *browser* e você verá que será _renderizado_ *"Hello World!"* na tela.

Note que não foi necessário realizar qualquer tipo de mapeamento manual para que o *endpoint "hello"* fosse publicado.
Ao receber uma requisição, o Thrust automaticamente tenta encontrar o módulo a partir do diretório *thrust/app*.

Devido ao fato de o arquivo *test.js* ter sido criado em na raíz do diretório *thrust/app*, ele automaticamente se torna acessível, bem como suas respectivas funções, publicadas através da variável de módulo *exports*.

#### Mapeando um *endpoint*
Conforme exemplificado anteriormente, não é necessário realizar qualquer tipo de mapeamento manual para que um módulo/endpoint se torne acessível. Contudo, muitas vezes é desejável mapear um módulo/endpoint de maneira manual para que se possa, por exemplo, encurtar/alterar sua URL.

Para realizar o mapeamento manual de um *endpoint* no ThrustJS, crie um diretório chamado *estoque* dentro de *thrust/app*.

Crie agora um outro diretório chamado *produtos* dentro de *thrust/app/estoque*.

Crie ainda um terceiro diretório chamado *comestiveis* dentro de *thrust/app/estoque/produtos*.

Crie um novo arquivo chamado *pereciveis.js* com o seguinte conteúdo:

```javascript
/**
 * Serviço que retorna uma lista de produtos perecíveis.
 * @param {Object} params - Objeto com os parâmetros enviados na requisição.
 * @param {http.Request} request - Wrapper da classe HttpServletRequest do Java.
 * @param {http.Response} response - Wrapper da classe HttpServletResponse do Java.
 */
function getProdutosPereciveis(params, request, response) {
    var listaProdutosPereciveis = [
        {
            nome: "Maracujá",
            quantidade: "2kg"
        },
        {
            nome: "Alcatra",
            quantidade: "5kg"

        }
    ]
    response.json(listaProdutosPereciveis);
}

exports = {
    getProdutosPereciveis: getProdutosPereciveis
}
```

Note que, ao salvar o arquivo *pereciveis.js*, já é possível chamar a URL http://localhost:8080/estoque/produtos/comestiveis/pereciveis/getProdutosPereciveis. Ao fazer isso, note que um JSON será retornado.
Temos então o *endpoint* getProdutosPereciveis publicado, mas note que a URL ficou extensa.

Vamos então mapear uma rota para uma URL mais curta.
Crie um arquivo chamado *routes.js* dentro do diretório thrust/app. A fim de manter o código organizado, vamos criar o arquivo *routes.js* em thrust/app/routes. Edite então o arquivo e escreva nele o seguinte conteúdo:

```javascript
exports = (function() {
    http.addRoute("/prod/getProdutosPereciveis", "/estoque/produtos/comestiveis/pereciveis/getProdutosPereciveis")
})()
```

No código acima, invocamos o método *addRoute* do objeto *http* (disponível por padrão no contexto global), passando dois parâmetros. O primeiro parãmetro indica a URI através da qual o *endpoint* será atendido. O segundo parâmetro indica o caminho real do *endpoint*.

Agora basta fazermos um require no módulo *routes* que acabamos de criar. Abra então o arquivo *startup.js* e adicione à ele o seguinte conteúdo:

```javascript
require(routes/routes)
```

Agora já é possível chamar a URL http://localhost:8080/prod/getProdutosPereciveis e obter o resultado da função *getProdutosPereciveis*.

Note que não foi necessário reinicializar o ThrustJS para que o novo arquivo *routes.js* fosse processado. Isso acontece porque o parâmetro *developmentMode* está ligado no arquivo *config.json*. Isso faz com que a cada nova chamada *REST* recebida, o ThrustJS recarregue os arquivos no diretório */thrust/core* e o arquivo *startup.js*, aumentando assim a produtividade durante o desenvolvimento. 

Consequentemente, ao desligar o parâmetro, tanto o conteúdo da pasta */thrust/core* quanto o arquivo *startup.js* serão carregador apenas uma vez (na inicialização do servidor).

#### Mapeando um módulo inteiro
Quando se tem muitos *endpoints* dentro de um módulo e desejamos mapeá-los *"as-is"*, ou seja, com os mesmos nomes das funções que os implementam, podemos adicionar uma rota apenas para o módulo, conforme o código abaixo:

```javascript
http.addRoute("/@pereciveis", "/estoque/produtos/comestiveis/pereciveis")
```

O caractere *@* indica que estamos mapeando, para aquela rota, o módulo todo. Sendo assim, o código acima disponibiliza todas as funções dentro do módulo *pereciveis* (arquivo pereciveis.js) como *endpoints*. Considerando ainda o exemplo anterior, a função *getProdutosPereciveis* estaria disponível para ser chamada, agora através da URL http://localhost:8080/@pereciveis/getProdutosPereciveis.

#### Criando/utilizando um módulo como *lib*. Chamamos isso de BitCode!
Um dos principais objetivos do ThrustJS é ser um Server-side JavaScript conciso e com poucas APIs, visando tornar a curva de aprendizado baixa e elevar o nível de produtividade.

Contudo, o ThrustJS oferece mecanismos de carregamento para que você crie e/ou utilize módulos adicionais.

Vamos criar um módulo chamado *mathematics* com algumas funções e disponibilizá-lo para uso.

Crie uma pasta chamada *lib* dentro de *thrust/app* e um arquivo chamado *mathematics.js* dentro da pasta *lib* criada.

Edite o arquivo *mathematics.js* com o seguinte conteúdo:

```javascript
function factorial(x) {
    if(x == 0) {
       return 1
    }
    return x * factorial(x-1)
 }

exports = {
    factorial: factorial
}
```

Com o módulo *mathematics* definido e implementado, basta importá-lo e torná-lo disponível para ser utilizado.

Para disponibilizar o módulo de maneira global, ou seja, torná-lo acessível de qualquer parte do ThrustJS, basta importá-lo para uma variável no arquivo *startup.js*. 

Edite então o arquivo *startup.js* e adicione o seguinte conteúdo:

```javascript
var mathematics = require("/lib/mathematics")
```

Pronto! O módulo está disponível para ser utilizado em qualquer lugar da sua aplicação.

#### Utilizando classes Java

No ThrustJS é possível utilizar classes Java, basicamente, de duas maneiras, conforme o código abaixo:

```javascript
function helloWorld() {
    //1st Way:
    var StringBuilder = Java.type("java.lang.StringBuilder")
    var sb = new StringBuilder("Hello ")

    //2nd Way:
    //var sb = new java.lang.StringBuilder("Hello ")

    sb.append("World")
    print(sb.toString())
}
```

Caso deseje utilizar classes Java que não acompanhem o *JDK*, basta adicionar o(s) arquivo(s) *.jar* com as classes no diretório /app/WEB-INF/lib e reiniciar o ThrustJS, para que o arquivo *.jar* seja carregado.

#### Performance

Para realizar testes de performance com o ThrustJS, certifique-se de que as confirugrações no arquivo *config.json* estejam conforme abaixo, a fim de obter máximo desempenho:

``` javascript
"reloadPlatform": false,
"developmentMode":  false,
"cacheScript": true
```

#### Suporte a HTTPS

Para habilitar/configurar o HTTPS no ThrustJS, basta inserir as informações no arquivo *server.properties*, conforme abaixo:

````
sslPort=8443
secure=true
scheme=https
keyAlias=youralias
keystorePass=yourpass
keystoreType=JKS
keystoreFile=keystore.jks
clientAuth=false
sslProtocol=TLS
maxThreads=200
protocol=org.apache.coyote.http11.Http11AprProtocol
SSLEnabled=true
```

Após fazer as devidas adaptações nas configurações, basta salvar o arquivo e reiniciar o ThrustJS!

#### *Debugging*
Para debugar o código *backend* no ThrustJS, inicialize o servidor com o parâmetro *agentlib* e suas opções conforme exemplo abaixo:

`java -agentlib:jdwp=server=y,suspend=y,transport=dt_socket,address=8888 -jar thrust.jar`

Digite a intrução de debug no local do código que deseja depurar:

`debugger;`

Inialize o NetBeans e siga as instruções contidas na URL <https://blogs.oracle.com/sundararajan/remote-debugging-of-nashorn-scripts-with-netbeans-ide-using-debugger-statements> e...

Divirta-se!

## *License*
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.