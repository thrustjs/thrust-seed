/**
 * Serviço que responde a URI _/test/hello_
 * @param {Object} params - Objeto com os parâmetros enviados na requisição.
 * @param {http.Request} request - Wrapper da classe HttpServletRequest do Java.
 * @param {http.Response} response - Wrapper da classe HttpServletResponse do Java.
 */
function hello (params, request, response) {
  response.write('Hello World!')
}

exports = {
  hello: hello
}
