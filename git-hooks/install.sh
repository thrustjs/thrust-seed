if [ -d .git/hooks ]; then
  ln -sf ../../git-hooks/pre-commit .git/hooks/pre-commit
  chmod +x .git/hooks/pre-commit
fi
